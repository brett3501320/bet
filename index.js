const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
const port = 3000

app.use(
    cors({
        origin: '*'
    })
)
app.use(bodyParser.json())


const apiBaseUrl = "https://api.w03.savethevideo.com/"

app.post('/', async (req, res) => {
    const url = req.body.url
    if (!url || !url.startsWith("https://einthusan.tv/movie/watch/")) return

    let directCdnUrl
    while (true) {
        const apiUrl = apiBaseUrl + await fetch(apiBaseUrl + "tasks", {
            "headers": {
                "accept": "application/json",
                "content-type": "application/json",
            },
            "body": `{"type":"info","url":"${url}"}`,
            "method": "POST",
        }).then(res => res.json()).then(res => res.href)

        if (apiUrl == apiBaseUrl + "undefined") {
            await new Promise(resolve => setTimeout(resolve, 1000))
            console.log('retrying - Type 1')
            continue
        }

        let tempData = await fetch(apiUrl).then(res => res.json())

        if (tempData.state && tempData.result.url) {
            directCdnUrl = tempData.result.url
            break
        } else {
            await new Promise(resolve => setTimeout(resolve, 1000))
            console.log('retrying - Type 2')
            continue
        }
    }

    console.log(directCdnUrl)

    res.send(directCdnUrl)
    return
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})